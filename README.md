# i18 CLI filler

This project is just a script to create/edit/fix translations files for a react project.

For translations i used a non-standard format because it fits react-i18next and it's easy for me:
`cat it.json`
```json
{
    "Hello world":"Ciao mondo"
}
```

## Instructions

1. **autoscan**
    > Search inside given path for <Trans>words</Trans> and add them to every dictionary automatically
1. **check words**
    > Sync every dictionary with every word
1. **list languages**
    > get a single line list of all read dictionaries
1. **add new language**
    > create a new dictionary with the given language code
1. **remove language**
    > remove dictionary from memory and its json file
1. **list words**
    > get a multi line list of every english word
1. **remove word**
    > remove the given word from every dictionary
1. **print version**
    > this script info
1. **exit**
    > give a save option before exit
1. **abort**
    > exit without saving

### License

MIT License

Copyright (c) 2020 Nicola Basso

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
