#!/usr/bin/env bash
FOLDER_PATH=../../i18n/*
PROJECT_PATH=../../../src

# jq cheatsheet: https://gist.github.com/olih/f7437fb6962fb3ee9fe95bda8d2c8fa4
# regex tested at: regexr.com/5f5sl
FIND_WORD_REGEX="(?<=(<Trans>))(.*?)((.|\n|\t|\r)*?)(?=(<\/Trans>))"
FIND_FILE_FORMAT="*.js"
DEFAULT_LANG=en

################################################################################
# utils
Bold=$(tput bold)
Normal=$(tput sgr0)

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'

DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color

# https://tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html
CURSORUP='\033[1A'
CURSORDOWN='\033[1B'
CURSORRESET='\033[2J'
CURSORSTART='\033[0;0H'
CURSORERASE='\033[K'

declare -A lang_array
declare -A blacklist_array
blacklist_file=""

util_save_json() {
    if [[ ! -d $1 ]]; then
        if [[ $1 == *.json ]]; then
            echo -e "Reading ${BLUE}${1}${NC} ..."
            # util_read_json $1
            lang_array[$(basename -s .json $1)]=$(cat $1)
        elif [[ $1 == *.blacklist ]]; then
            echo -e "${GREEN}Found a .blacklist file!${NC} Reading it ..."
            blacklist_file=$1
            readarray -t b_array < $1
            i=0
            for val in "${b_array[@]}"; do
                blacklist_array[$i]=$val
                i=$i+1
            done
        fi
    fi
}

util_read_json() {
    declare -A words_array
    len=$(jq '. | length' $1)
    for (( i = 0; i < len; i++ )); do
        single_key=$(jq '. | keys | .['"$i"']' $1)
        val=$(jq '.['"$single_key"']' $1)
        words_array[$single_key]=$val
        printf "${CURSORERASE}${CYAN}%s${NC} = ${GREEN}%s${NC}\n" "${single_key}" "$val"
        echo -e "${CURSORERASE}\n${CURSORERASE}\n${CURSORERASE}\n"
        echo -e "\033[1;0H"
        for x in "${!words_array[@]}"; do
            printf "$CYAN%s$NC = $GREEN%s$NC\n" "$x" "${words_array[$x]}"
        done
        echo ""
    done
}

util_scan_project() {
    local choice=$PROJECT_PATH
    if [[ $(echo -e $1 | wc -c) > 1 ]]; then
        choice=$1
    fi
    echo -e "Scanning ${LIGHTCYAN}$choice${NC} ..."
    if [[ ! -d $choice ]]; then
        echo -e "${RED}Directory not found!${NC}"
        return
    fi
    #search in every file
    files=$(find $choice -type f \( -iname $FIND_FILE_FORMAT \))
    for file in $files ; do
        current=$(cat -s $file)
        reg=$(echo $current | grep -oP $FIND_WORD_REGEX)
        readarray -t result <<< "$reg"
        for newword in "${result[@]}" ; do
            if [[ ${#newword} > 0 ]]; then
                echo -e "${CURSORERASE}Adding ${GREEN}\"$newword\"${NC} to every dictionary..."
                util_add_word "$newword"
            fi
        done
    done
}

util_list_json() {
    for i in ${!lang_array[@]}; do
        echo -e "\t\t\t\t${Bold}$i${Normal}"
        echo -e ${lang_array[$i]} | jq '.'
        echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
    done
}

util_get_languages() {
    langs=${!lang_array[*]}
    echo $langs
}

util_get_words() {
    dict=${lang_array[$DEFAULT_LANG]}
    keys=$(echo $dict | jq '. | keys' | awk '{$1=$1};1')
    # remove [ ] and new lines after that
    keys=${keys:2:-2}
    echo -e "$CYAN$keys$NC"
}

util_create_lang() {
    echo -e "Creating new translation from ${LIGHTGREEN}$DEFAULT_LANG${NC} to ${LIGHTGREEN}$1${NC}"
    if [[ -z ${lang_array[$1]} ]]; then
        json=${lang_array[$DEFAULT_LANG]}
        lang_array[$1]=${lang_array[$DEFAULT_LANG]}
    fi
}

util_add_word() {
    word=$1
    word=${word//\"/\'} # escaping "
    # for every saved json
    for i in ${!lang_array[@]}; do
        dict=${lang_array[$i]}
        keys=$(echo $dict | jq '. | keys' | awk '{$1=$1};1')
        #check if word already exists
        if [[ "$keys" == *"$word"* ]]; then
            echo -e "${CURSORERASE}${YELLOW}$i: String already saved!${NC}${CURSORUP}"
            continue
        fi
        # add at the end a new value pair
        newpair='{"'$word'":"'$word'"}'
        echo -e "${CURSORERASE}Adding new entry to ${GREEN}$i${NC}: ${BLUE}\"$word\"${NC}${CURSORUP}"
        currentjson=${lang_array[$i]}
        filter=". |= . + $newpair"
        result=$(echo $currentjson | jq ''"$filter"'')
        lang_array[$i]=$(echo $result)
    done
}

util_fix_translations() {
    local translated
    # already_checked=()
    # check on every language file ignoring "en"
    langs=$(util_get_languages)
    IFS=' ' read -r -a langs_arr <<< "$langs"

    # manual translation
    if ! command -v trans >/dev/null 2>&1 ; then
        echo -e "${RED}Missing dependency: trans${NC}"
        echo -e "use 'sudo apt install translate-shell' \n\n"
        echo -e "${YELLOW}${Bold}Falling back to manual translation ...${Normal}${NC}"
        for element in "${langs_arr[@]}" ; do
            if [[ $element == $DEFAULT_LANG ]]; then
                continue
            fi
            echo -e "If required, insert manual translation of non translated strings from ${LIGHTCYAN}$DEFAULT_LANG${NC} to ${BLUE}$element${NC}"
            # for every {word : word}, skip {word : parola}
            keys=$(echo "${lang_array[$element]}" | jq '. | keys')
            len=$(echo "${lang_array[$element]}" | jq '. | length')
            for (( i = 0; i < len; i++ )); do
                single_key=$(echo "${lang_array[$element]}" | jq '. | keys | .['"$i"']')
                jqfil=".[$single_key]"
                val=$(echo "${lang_array[$element]}" | jq ''"$jqfil"'')
                if [[ $single_key == $val ]]; then
                    word=$(echo $single_key)
                    read -p "$word: " translated
                    if [[ ${#translated} > 0 ]]; then
                        key2="$translated"
                        filter=".[$single_key] = \"$key2\""
                        lang_array[$element]=$(echo "${lang_array[$element]}" | jq ''"$filter"'')
                    fi
                fi
            done
        done
    # automatic translation
    else
        for element in "${langs_arr[@]}" ; do
            if [[ $element == $DEFAULT_LANG ]]; then
                continue
            fi
            retries=0
            # for every {word : word}, skip {word : parola}
            keys=$(echo "${lang_array[$element]}" | jq '. | keys')
            len=$(echo "${lang_array[$element]}" | jq '. | length')
            for (( i = 0; i < len; i++ )); do
                single_key=$(echo "${lang_array[$element]}" | jq '. | keys | .['"$i"']')
                val=$(echo "${lang_array[$element]}" | jq '.['"$single_key"']')
                # translate word if not already done
                if [[ $single_key == $val ]]; then
                    word=$(echo $single_key)
                    word_trim=${word:1:-1}
                    echo -e "Translating \"${GREEN}$word_trim${NC}\" from ${LIGHTCYAN}$DEFAULT_LANG${NC} to ${BLUE}$element${NC} ..."
                    # follow blacklist rules before using internet services
                    blacklisted=0
                    for val in "${blacklist_array[@]}"; do
                        if [[ "${val}" == "${word_trim}" ]]; then
                            blacklisted=1
                        fi
                    done
                    if [[ $blacklisted -eq 1 ]]; then
                        echo -e "${YELLOW} Blacklisted word!${NC} Not traslating it..."
                    else
                        # using internet dictionaries translation
                        word_trans=$(trans -l it -s $DEFAULT_LANG -t $element "$word_trim" -show-translation Y -show-original n -show-dictionary n -show-translation-phonetics n -show-prompt-message n -show-languages n -show-original-dictionary n -show-alternatives n -no-ansi -no-auto | tr -d '\n')
                        word_trans=$(echo -e "${word_trans}" | tr -d \")
                        if [[ ${#word_trans} > 0 ]]; then
                            if [[ $word_trim != $word_trans ]]; then
                                echo "Translation found: \"$word_trans\" "
                                key2="$word_trans"
                                filter=".[$single_key] = \"$key2\""
                                lang_array[$element]=$(echo "${lang_array[$element]}" | jq ''"$filter"'')
                            else
                                echo -e "${YELLOW}Translation for ${ORANGE}$word${NC} is the same as the original!${NC}"
                                # add word to optional blacklist
                                already_checked+=("${word_trim}")
                            fi
                        else
                            echo -e "${RED}Translation for ${ORANGE}$word${NC} not found! (got ${ORANGE}\"$word_trans\"${NC} instead)${NC}"
                            # break after tot retries
                            retries=$((retries+1))
                            if [[ $retries -eq 10 ]]; then
                              echo -e "\n${RED}Reached 10 retries for this language! Skipping to the next...${NC}\n"
                              i=$((len))
                            fi
                        fi
                    fi
                fi
            done
        done
        if [[ "${#already_checked[@]}" -gt 1 ]]; then
            for val in "${blacklist_array[@]}"; do
                already_checked+=("${val}")
            done
            cleared_array=$(printf "%s\n" "${already_checked[@]}" | sort -u)
            echo -e "${cleared_array[@]}"
            echo -e "${YELLOW}Include these words into the blacklist file (${blacklist_file})?${NC} [y/N]"
            read affirmative
            if [[ $affirmative == "Y" || $affirmative == "y" ]]; then
                echo -e "" > $blacklist_file # reset blacklist file
                for val in "${cleared_array[@]}"; do
                    echo -e "${val}" >> $blacklist_file
                done
                echo -e "${Bold}Words added to blacklist!${Normal}${NC}"
            fi
        fi
    fi
}

util_remove_lang() {
    echo -e "Removing dictionary for ${Bold}${RED}$1${NC}${Normal}"
    unset lang_array["$1"]
    remjson="${FOLDER_PATH%?}$1.json"
    if [[ -f $remjson ]]; then
        echo -e "Removing file $remjson"
        rm $remjson
    fi
}

util_remove_word() {
    words_array=()
    echo -e "Select which word you want to remove:"
    len=$(echo "${lang_array[$DEFAULT_LANG]}" | jq '. | length')
    for (( i = 0; i < len; i++ )); do
        single_key=$(echo "${lang_array[$DEFAULT_LANG]}" | jq '. | keys | .['"$i"']')
        words_array+=("$single_key")
    done
    select choice in "${words_array[@]}" ; do
        echo -e "Are you sure to remove \"${YELLOW}$choice${NC}\" ? [y/N]"
        read affirmative
        if [[ $affirmative == "Y" || $affirmative == "y" ]]; then
            echo -e "${RED}Removing \"$choice\" from every dictionary...${NC}"
            key="${choice:1:-1}"
            filter="del(.\"$key\")"
            for i in ${!lang_array[@]}; do
                lang_array[$i]=$(echo "${lang_array[$i]}" | jq ''"$filter"'')
            done
        fi
        break
    done
}

util_write_json() {
    newjson="${FOLDER_PATH%?}$1.json"
    echo -e "${LIGHTGREEN}lang${NC}: $1"
    if [[ ! -f "$newjson" ]]; then
        echo -e "${BLUE}Creating new language${NC}: $newjson"
        touch $newjson
    else
        echo -e "${YELLOW}Overwriting $LIGHTGREEN$newjson$NC ..."
    fi
    if [[ ! -w "$newjson" ]]; then
        echo -e "${RED}Missing write permission! Aborting...$NC"
        exit 2
    fi

    # write to file
    if [[ -f "$newjson" ]]; then
        echo -e "Writing to $GREEN$newjson$NC the updated json..."
        echo -e "${lang_array[$1]}" > "$newjson"
    fi
}

util_langs_to_files() {
    langs=$(util_get_languages)
    IFS=' ' read -r -a langs_arr <<< "$langs"
    for element in "${langs_arr[@]}" ; do
        util_write_json $element
    done
}

util_scan_n_fix() {
    langs=$(util_get_languages)
    IFS=' ' read -r -a langs_arr <<< "$langs"

    def_dict=${lang_array[$DEFAULT_LANG]}
    def_keys=$(echo $def_dict | jq '. | keys' | awk '{$1=$1};1')
    def_keys=${def_keys:2:-2}

    for index in "${!langs_arr[@]}" ; do
        current_lng="${langs_arr[index]}"
        if [[ "$DEFAULT_LANG" = "$current_lng" ]]; then
            continue
        fi
        new_dict=${lang_array["${langs_arr[index]}"]}
        new_keys=$(echo $new_dict | jq '. | keys' | awk '{$1=$1};1')
        new_keys=${new_keys:2:-2}
        # fix missing words inside new_dict
        if [[ "$def_keys" != "$new_keys" ]]; then
            readarray -t def_arr <<< "$def_keys"
            readarray -t new_arr <<< "$new_keys"
            diff_arr=(`echo ${def_arr[@]} ${new_arr[@]} | tr '",' '\n' | sort | uniq -u `)
            echo -e "${RED}Found missing words! Fixing them... $NC"
            echo -e "def: $CYAN"
            for element in "${def_arr[@]}" ; do
                echo "$element"
            done
            echo -e "\t\t\t   $RED ! = $NC"
            echo -e "new: $CYAN"
            for element2 in "${new_arr[@]}" ; do
                echo "$element2"
            done
            echo -e "$NC"
            for element in "${diff_arr[@]}" ; do
                util_add_word "$element"
            done
        fi
    done
}

################################################################################

print_menu() {
    clear
    echo -e "\t\t\t  ${Bold}i18n JSON filler${Normal}"
    echo -e "${LIGHTGREEN}1${GREEN})${NC} autoscan"
    echo -e "${LIGHTGREEN}2${GREEN})${NC} check and translate words"
    echo -e "${LIGHTGREEN}3${GREEN})${NC} list languages"
    echo -e "${LIGHTGREEN}4${GREEN})${NC} add new language"
    echo -e "${LIGHTGREEN}5${GREEN})${NC} remove language"

    echo -e "${LIGHTGREEN}6${GREEN})${NC} list words"
    echo -e "${LIGHTGREEN}7${GREEN})${NC} add new word"
    echo -e "${LIGHTGREEN}8${GREEN})${NC} remove word"

    echo -e "${LIGHTGREEN}9${GREEN})${NC} print version"
    echo -e "${LIGHTGREEN}0${GREEN})${NC} ${Bold}EXIT${Normal}"
    echo -e "${LIGHTRED}q${RED})${NC} ${Bold}ABORT${Normal}"
}

read_menu_input() {
    local choice
    read -r -p "Option: " choice
    case $choice in
        1) # search for missing translations over the entire project using regex
            read -p "Insert project path: [Enter for ($PROJECT_PATH)] " p_path
            echo -e "Autoscanning project structure..."
            util_scan_project $p_path
            echo -e "${CURSORERASE}${Bold}Scan finished!${Normal}"
            ;;
        2) # find for missing words on other dictionaries
            echo -e "Searching and adding missing words..."
            util_scan_n_fix
            echo -e "Searching for missing translations..."
            util_fix_translations
            ;;
        3) # list all translation filenames
            local langs
            langs=$(util_get_languages)
            echo -e "$CYAN$langs$NC"
            ;;
        4) # copy en.json file and replace translations with ""
            echo -e "Use one of the following ${Bold}shortcodes${Normal} to select a new language:"
            trans -R
            read -p "(Press [Enter] to cancel) : " short_c
            if [[ ${#short_c} > 0 ]]; then
                util_create_lang $short_c
                echo -e "New language \"$short_c\" added."
            fi
            ;;
        5) # remove one or more languages
            local ch
            echo -e "Saved languages:"
            langs=$(util_get_languages)
            select ch in $langs; do
                echo -e "Are you sure to remove ${YELLOW}$ch${NC} ? [y/N]"
                read affirmative
                if [[ $affirmative == "Y" || $affirmative == "y" ]]; then
                    util_remove_lang $ch
                    echo -e "${YELLOW}${Bold}$ch${Normal}${NC} language removed!"
                else
                    echo -e "${GREEN}${Bold}$ch${Normal}${NC} language preserved..."
                fi
                break
            done
            ;;
        6) # list all translated words
            util_get_words
            ;;
        7) # open each file and add at the end ', { "new word":"translation" }'
            read -p "Write the word/phrase you want to add: " add_w
            if [[ ${#add_w} > 0 ]]; then
                util_add_word "$add_w"
                echo -e "New word \"$add_w\" added."
            fi
            ;;
        8) # search for "word" and remove "word":"translation", on every file
            util_remove_word
            ;;
        9) # show project info
            print_info true
            echo -e ""
            ;;
        0) # exit from script
            save_n_exit
            ;;
        q) # exit without saving
            exit 0 && clear
            ;;
        *) # invalid argument
            return
            ;;
    esac

    read -p "Press [Enter] to continue... "
}
# echo -e "${CURSORRESET}${CURSORSTART}"
print_info() {
    if [[ $1 = false ]]; then
        clear
    fi
    echo -e "                          - - - - - - - - - -                     "
    echo -e "                           i18n JSON filler                       "
    echo -e "                          - - - - - - - - - -                     "
    echo -e "${RED}${Bold}Notice:${Normal}${NC}"
    echo -e "English based translations, aka from english to other languages"
    echo -e "${RED}${Bold}Notice pt2:${Normal}${NC}"
    echo -e "this script is created to be used with a project"
    echo -e "with ${YELLOW}react-i18next${NC} npm package"
    echo -e "\tfrom ${BLUE}https://github.com/i18next/react-i18next${NC}"
    echo -e "${RED}${Bold}Script info:${Normal}${NC}"
    echo -e "version: 0.3"
    echo -e "release: ${YELLOW}dev${NC}"
    echo -e "\tfrom ${BLUE}https://gitlab.com/nicola.basso/i18n-cli-filler${NC}"
    if [[ $1 = false ]]; then
        echo -e "\n\n\n"
    fi
}

# prompt saving option
save_n_exit() {
    local choice
    echo -e "Saving following jsons:"
    util_list_json
    echo -e "Confirm?"
    select choice in "Yes" "No" "Back"; do
        case $choice in
            Yes ) util_langs_to_files; read -p "Saving done, bye "; exit 0;;
            No ) exit 0;;
            Back ) return;;
        esac
    done
}

# ask for user config
user_config() {
    echo -e "User setup"
    echo -e "FOLDER_PATH: ${GREEN}${Bold}$FOLDER_PATH${Normal}${NC}"
    echo -e "Do you want to change default ${GREEN}${Bold}FOLDER_PATH?${Normal}${NC}"
    read -p "[Enter] to skip " fpath
    if [[ ${#fpath} > 0 ]]; then
        FOLDER_PATH=$fpath
    fi
    echo -e "PROJECT_PATH: ${BLUE}${Bold}$PROJECT_PATH${Normal}${NC}"
    echo -e "Do you want to change default ${BLUE}${Bold}PROJECT_PATH?${Normal}${NC}"
    read -p "[Enter] to skip " ppath
    if [[ ${#ppath} > 0 ]]; then
        PROJECT_PATH=$ppath
    fi
    echo -e "\n"
}

################################################################################

# main part of the script
main() {
    if ! command -v jq >/dev/null 2>&1 ; then
        echo -e "${RED}Missing dependency: jq${NC}"
        echo -e "use 'sudo apt install jq'"
        exit 1
    fi
    print_info false
    user_config
    # first save
    for file in $FOLDER_PATH ; do
        util_save_json $file
    done
    echo -e "Reading from files finished!"
    read -p "Press [Enter] to open menu..."
    # main menu loop
    while true
    do
        read_folder
        print_menu
        read_menu_input
    done
    exit 0
}

################################################################################

# CLI mode
if [[ "$#" > 0 ]]; then
    for argval in "$@" ; do
        case $argval in
            "-h" )
                echo -e "\t\t\t  ${BLUE}${Bold}i18n JSON filler${Normal}${NC}"
                echo -e "\t ${GREEN}-h${NC} \t show this ${LIGHTGREEN}h${NC}elp."
                echo -e "\t ${GREEN}-i${NC} \t print script ${LIGHTGREEN}i${NC}nfo."
                echo -e "\t ${GREEN}-s${NC} \t ${LIGHTGREEN}s${NC}can for jsons. ${RED}REQUIRED BEFORE -f -t -w -d ${NC}!"
                echo -e "\t ${GREEN}-f${NC} \t ${LIGHTGREEN}f${NC}ix missing words on every json."
                echo -e "\t ${GREEN}-t${NC} \t ${LIGHTGREEN}t${NC}ranslate jsons according to file name."
                echo -e "\t ${GREEN}-w${NC} \t ${LIGHTGREEN}w${NC}rite translations to file."
                echo -e "\t ${GREEN}-d${NC} \t ${LIGHTGREEN}d${NC}elete json language."
                echo -e "\tRecommended parameters: ${Bold}-sftw${Normal}"
                echo -e "No parameters = TUI interface"
                ;;
            "-i" )
                print_info true
                ;;
            "-s" )
                echo -e "${Bold}Scan${Normal}"
                util_scan_project
                ;;
            "-f" )
                echo -e "${Bold}Fix${Normal}"
                util_scan_n_fix
                ;;
            "-t" )
                echo -e "${Bold}Translate${Normal}"
                util_fix_translations
                ;;
            "-w" )
                echo -e "${Bold}Save${Normal}"
                util_langs_to_files
                ;;
            "-d" )
                echo -e "${Bold}Remove language${Normal}"
                langs=$(util_get_languages)
                select ch in $langs; do
                    echo -e "Are you sure to remove ${YELLOW}$ch${NC} ? [y/N]"
                    read affirmative
                    if [[ $affirmative == "Y" || $affirmative == "y" ]]; then
                        util_remove_lang $ch
                        echo -e "${YELLOW}${Bold}$ch${Normal}${NC} language removed!"
                    fi
                    break
                done
                ;;
            "-r" )
                echo -e "${Bold}Remove word${Normal}"
                util_remove_word
                ;;
            "-sftw" )
                echo -e "${Bold}Scan${Normal}"
                util_scan_project
                echo -e "${Bold}Fix${Normal}"
                util_scan_n_fix
                echo -e "${Bold}Translate${Normal}"
                util_fix_translations
                echo -e "${Bold}Save${Normal}"
                util_langs_to_files
                ;;
        esac
    done
    exit 0
fi
# TUI mode
main
